require recipes-ti/bios/ti-sysbios.inc

PV = "6_41_04_54"
PR = "r0"

LIC_FILES_CHKSUM = "file://bios_${PV}_manifest.html;md5=95c3c6050fb23f2755ab75bf76805190"

SRC_URI[sysbiosbin.md5sum] = "cbc53f2b15c2f70e2eaa05c1ed90b8db"
SRC_URI[sysbiosbin.sha256sum] = "2b4fcb85af4c0abc46dadc925312b2a28906ec34db9b1500f0c484ee3fcb58cf"

TI_BIN_UNPK_CMDS=""
TI_BIN_UNPK_ARGS = "--mode unattended --prefix ${WORKDIR}"

FILES_${PN} += "${SYSBIOS_INSTALL_DIR_RECIPE}"

INSANE_SKIP_${PN} = "arch ldflags staticdev"
