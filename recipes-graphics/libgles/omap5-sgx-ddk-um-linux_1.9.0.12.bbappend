BRANCH = "next"

SRC_URI = "git://git.ti.com/graphics/omap5-sgx-ddk-um-linux.git;protocol=git;branch=${BRANCH}"
SRCREV = "b6e57ccf36fc8c36d988246bc7510f0dec42d991"

PR_append = ".tisdk0"
